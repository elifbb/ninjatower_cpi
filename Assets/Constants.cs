﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Analytics;

namespace Ernie.Core
{
    public static class Constants
    {
	    public const int TRUE_INTEGER_VALUE = 1;
	    public const int FALSE_INTEGER_VALUE = 0;

		public const string TAG_KEY_SEPARATOR = "_";
		public const string ACHIEVEMENTS_PREFIX = "Achievements";
	    public const string APPLICATION_SETTING = "AppSetting";
	    public const string OPTIONS = "Options";
	    public const string REMOTE_CONFIG_PREFIX = "RC";//TIED TO FIREBASE EVENTS! DO NOT CHANGE

	    public const int DUELING_GAME_ID = 0;
	    public const int TEST_GAME_ID = -111;

	    public const string EVENT_SHOW_ANNOUNCEMENT = "event_show_announcement";
	    
		public static class Rewarding
        {
        }

        public static class Options
        {
	        public const string QUALITY_SETTING_INDEX_KEY_NAME = "QualitySetting";
			public const int DEFAULT_VIBRATION_ACTIVE_VALUE = TRUE_INTEGER_VALUE;
            public const int DEFAULT_SOUND_ACTIVE_VALUE = TRUE_INTEGER_VALUE;
            public const string VIBRATION_ACTIVE_KEY = APPLICATION_SETTING + TAG_KEY_SEPARATOR + "Vibration";
            public const string SOUND_ACTIVE_KEY = APPLICATION_SETTING + TAG_KEY_SEPARATOR + "Sound";

            public const int DEFAULT_QUALITY_SETTING_INDEX = 1;

            public const string CAMERA_SHAKE_COEFFICIENT_INDEX_KEY_NAME = APPLICATION_SETTING + TAG_KEY_SEPARATOR + "CameraShakeCoefficientIndex";
            public const int DEFAULT_CAMERA_SHAKE_COEFFICIENT_INDEX = 1;

            public const string PUSH_NOTIFICATION_REQUEST_COUNT_KEY = APPLICATION_SETTING + TAG_KEY_SEPARATOR + "PushNotificationRequestCount";

            public static void UpdateWithRemoteConfigValues()
            {
				throw new NotImplementedException();
            }

			public static int CameraShakeCoefficientIndex
            {
                get
                {
                    return PlayerPrefs.GetInt
                    (
                        CAMERA_SHAKE_COEFFICIENT_INDEX_KEY_NAME,
                        DEFAULT_CAMERA_SHAKE_COEFFICIENT_INDEX
                    );
                }
                set
                {
                    PlayerPrefs.SetInt
                    (
                        CAMERA_SHAKE_COEFFICIENT_INDEX_KEY_NAME,
                        value
                    );
                }
            }

            public static Single ComputeCameraShakeCoefficient
            {
                get
                {
                    int coefficientIndex = CameraShakeCoefficientIndex;
                    if (coefficientIndex <= 0)
                    {
                        return 0.5f;
                    }
                    else if (coefficientIndex == 1)
                    {
                        return 1.0f;
                    }
                    else
                    {
                        return 1.5f;
                    }
                }
            }

            public static Boolean VibrationActive
            {
                get
                {
                    return PlayerPrefs.GetInt
                    (
                        VIBRATION_ACTIVE_KEY,
                        DEFAULT_VIBRATION_ACTIVE_VALUE
                    ) != FALSE_INTEGER_VALUE;
                }
                set
                {
                    PlayerPrefs.SetInt
                    (
                        VIBRATION_ACTIVE_KEY,
                        value ? TRUE_INTEGER_VALUE : FALSE_INTEGER_VALUE
                    );
                }
            }

            public static Boolean SoundActive
            {
                get
                {
                    return PlayerPrefs.GetInt
                    (
                        SOUND_ACTIVE_KEY,
                        DEFAULT_SOUND_ACTIVE_VALUE
                    ) != FALSE_INTEGER_VALUE;
                }
                set
                {
                    PlayerPrefs.SetInt
                    (
                        SOUND_ACTIVE_KEY,
                        value ? TRUE_INTEGER_VALUE : FALSE_INTEGER_VALUE
                    );
                }
            }

            public static void ApplySound()
            {
                AudioListener.pause = !SoundActive;
            }

            public static Int32 QualitySettingIndex
            {
	            get
	            {
		            return PlayerPrefs.GetInt
		            (
			            QUALITY_SETTING_INDEX_KEY_NAME,
			            DEFAULT_QUALITY_SETTING_INDEX
		            );
	            }
	            set
	            {
		            PlayerPrefs.SetInt
		            (
			            QUALITY_SETTING_INDEX_KEY_NAME,
			            value
		            );
	            }
            }
		}

        public static class Scene
        {
            public const string GAME_SCENE_NAME = "GameScene";
            public const string LEVEL_TRANSITION_SCENE_NAME = "LevelTransition";
        }

        public static class PlayerProfile
        {
	        public const string PLAYER_PROFILE = "PlayerProfile";
	        public const string CURRENT_LEVEL_ID = "CurrentLevelId";
	        public const string UNLOCKED_LEVEL_ID = "UnlockedLevelId";

	        public static int GetCurrentLevelId(int gameId)
	        {
		        return PlayerPrefs.GetInt(MakeGameKey(gameId) + CURRENT_LEVEL_ID, 0);
	        }

	        public static void SetCurrentLevelId(int gameId, int levelId)
	        {
		        PlayerPrefs.SetInt(MakeGameKey(gameId) + CURRENT_LEVEL_ID, levelId);
	        }

			public static string MakeLevelKey(int gameId, int levelId)
	        {
		        return MakeGameKey(gameId) + TAG_KEY_SEPARATOR + levelId;
	        }

	        public static string MakeGameKey(int gameId)
	        {
		        return "Game" + TAG_KEY_SEPARATOR + gameId;
	        }
		}
        
        public static class InAppPurchases
        {
            public const string REMOVE_ADS_KEY = APPLICATION_SETTING + TAG_KEY_SEPARATOR + "removeads";
        
            public static Boolean RemoveAds
            {
                get
                {
                    return PlayerPrefs.GetInt
                    (
                        REMOVE_ADS_KEY,
                        0
                    ) == 1;
                }
                set
                {
                    if (value)
                    {
                        PlayerPrefs.SetInt
                        (
                            REMOVE_ADS_KEY,
                            1
                        );
                    }
                }
            }

            public static Boolean isAdsRemoved
            {
	            get => RemoveAds;
            }
        }

        public class Achievements
        {
            public const string ACHIEVEMENT_SAMPLE_ACHIEVEMENT = ACHIEVEMENTS_PREFIX + TAG_KEY_SEPARATOR + "SampleAchievement";

			public static bool IsAchievementRewarded(string key)
			{
				return PlayerPrefs.GetInt(key) == TRUE_INTEGER_VALUE;
			}

			public static void SetAchievementRewarded(string key, int value)
			{
				if (value <= 0) value = FALSE_INTEGER_VALUE;
				else if (value >= 1) value = TRUE_INTEGER_VALUE;
				PlayerPrefs.SetInt(key, value);
			}
        }

        public class AnalyticEvents
        {
	       public const string USER_RESPONDED_TO_PUSH_NOTIFICATION = "User_Responded_To_Notification";
            
	        public const string AD_INTERSTITIAL_SHOW_KEY = "Ad_Instl_Show";
	        public const string AD_INTERSTITIAL_REQUEST_KEY = "Ad_Instl_Request";
	        public const string AD_REWARDED_SHOWED_KEY = "Ad_Rwd_Show";
	        public const string AD_REWARDED_CLICKED_KEY = "Ad_Rwd_Clicked";
	        public const string AD_REWARDED_DISMISSED_KEY = "Ad_Rwd_Dismissed";

            public const string LEVEL_COMPLETED_KEY = "Level Complete";
            public const string LEVEL_SKIPPED_KEY = "Level_Skipped";
            public const string LEVEL_QUIT_KEY = "Level_Quit";
	        public const string LEVEL_FAILED_KEY = "Level_Failed";
	        public const string LEVEL_STARTED_KEY = "Level Started";

	        public const string TEST_EVENT_KEY = "fbTest";
		}

        public class RemoteConfig
        {
			
            //TIED TO FIREBASE EVENTS! DO NOT CHANGE
			public const string MINIMUM_PARKOUR_TO_SHOW_INTERSTITIAL_ADS_KEY = REMOTE_CONFIG_PREFIX + TAG_KEY_SEPARATOR + "InterAdStartLevel";

			//TIED TO FIREBASE EVENTS! DO NOT CHANGE
			public const string INTERSTITIAL_AD_INTERVAL_KEY = REMOTE_CONFIG_PREFIX + TAG_KEY_SEPARATOR + "InterAdInterval";

			//TIED TO FIREBASE EVENTS! DO NOT CHANGE
			public const string PUSH_NOTIFICATION_WINDOW_START_HOUR_KEY = REMOTE_CONFIG_PREFIX + TAG_KEY_SEPARATOR + "PushNtfWindowStartHour";

			//TIED TO FIREBASE EVENTS! DO NOT CHANGE
			public const string PUSH_NOTIFICATION_WINDOW_END_HOUR_KEY = REMOTE_CONFIG_PREFIX + TAG_KEY_SEPARATOR + "PushNtfWindowEndHour";
            
            //TIED TO FIREBASE EVENTS! DO NOT CHANGE
            public const string PUSH_NOTIFICATION_ENABLE_AT_LEVEL_KEY = REMOTE_CONFIG_PREFIX + TAG_KEY_SEPARATOR + "PushNtfEnableAtLevel";
			
		}
    }
}
