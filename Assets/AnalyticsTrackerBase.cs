﻿using System;
using Ernie.Core;
using Ernie.Extensions;
using UnityEngine;

namespace Assets.Ernie.Integration
{
	public abstract class AnalyticsTrackerBase:MonoBehaviour
	{
		public virtual void SendUserRespondedToLocalNotificationEvent(string notificationName, int notificationHour, int notificationMinute)
		{
			Extensions.LogAsNotImplemented();
		}

		public virtual void SendRewardedAdDisplayedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_SHOWED_KEY);
		}

		public virtual void SendRewardedAdClickedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_CLICKED_KEY);
		}

		public virtual void SendRewardedAdDismissedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_DISMISSED_KEY);
		}

		public virtual void SendShowInterstitialAdEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_INTERSTITIAL_SHOW_KEY);
		}

		///<summary>
		///this is called when a request to show an interstitial ad is made.
		///It is not guaranteed that the request will be fulfilled
		///</summary>
		public virtual void SendShowInterstitalAdRequestEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_INTERSTITIAL_REQUEST_KEY);
		}

		public abstract void SendTestEvent();

		/// <summary>
		/// Sent when the user successfully finishes a level (within time requirements, or as first player to finish, etc
		/// </summary>
		/// <param name="currentLevelPass"></param>
		public abstract void SendLevelCompleteEvent(int levelNo);

		public abstract void SendLevelSkippedEvent(int levelNo);

		/// <summary>
		/// Sent when the user quits a level 
		/// </summary>
		/// <param name="currentLevelPass"></param>
		public abstract void SendLevelQuitEvent(int levelNo);

		/// <summary>
		/// Send an event when a new level starts. Currently this does not hold the user's
		/// level chronometer start time, but  some time around when level scene load is completed
		/// </summary>
		/// <param name="currentLevelPass">LevelDescription object. Need ShortName and StageIndex from it to determine the level.</param>
		public abstract void SendLevelStartEvent(int levelNo);

		/// <summary>
		/// Sent when the user fails to complete a level within requirements.
		/// User may fail to finish within given time etc.
		/// Some mini games do not have level failure
		/// </summary>
		/// <param name="currentLevelPass">LevelDescription object. Need ShortName and StageIndex from it to determine the level.</param>
		public abstract void SendLevelFailEvent(int levelNo);

		public abstract void SendRewardedTypeEvent(string rewardType, int levelNo);
		protected string GetLevelName(int levelNo)
		{
			return "" + levelNo;
		}

		protected abstract void SendEvent(string name);

		public void CrashApp()
		{
			throw new Exception("Crash Test Exception");
		}

		protected virtual void ShowTrackerNotEnabledMessage()
		{
			throw new NotImplementedException();
		}
	}
}
