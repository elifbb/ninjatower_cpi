using System.Collections;
using System.Collections.Generic;
using Ernie.Integration;
using UnityEngine;
using UnityEngine.UI;

public class HeroController : MonoBehaviour
{
    public GameObject mainCam;
    public bool levelActive;
    public GameObject hero;
    public float rotationSpeed;
    public float jumpSpeed;
    
    public GameObject scroll;
    public GameObject tutorialText;

    public GameObject[] levels;
    public GameObject[] levelAxis;
    public GameObject[] levelSpawns;
    public GameObject[] levelTurns;
    public GameObject[] levelScrolls;
    public ParticleSystem ninjaSmoke;

    private float idleDuration;
    private bool jumping;
    private float currentY;
    private float nextY;
    private float nextCamY;
    private GameObject currentSpring;

    private bool falling;
    private bool kicked;
    private GameObject currentEnemy;

    private bool levelend;
    private Quaternion _lookRotation;
    private Vector3 _direction;
    private float RotationSpeed;

    private int levelId;
    public int _compoundLevelIndex { get; private set; }
    private GameObject currentSpawn;
    private GameObject currentTurn;
    private Vector3 camPosition;



    // Start is called before the first frame update
    void Start()
    {
        //turn mesh off on spawn and turns
        for(int i=0; i<levelSpawns.Length; i++)
        {
            levels[i].SetActive(false);
            levelSpawns[i].GetComponent<MeshRenderer>().enabled = false;
            levelTurns[i].GetComponent<MeshRenderer>().enabled = false;
            levelScrolls[i].GetComponent<MeshRenderer>().enabled = false;
        }

        
        
        

        idleDuration = 2.0f;
        levelId = 0;
        _compoundLevelIndex = 0;
        levelActive = false;
        
        jumpSpeed = 10.0f;
        RotationSpeed = 10.0f;
        
        StartCoroutine(ShowTutorial());
        StartLevel();
    }

    public void StartLevel()
    {
        levels[levelId].SetActive(true);
        jumping = false;
        falling = false;
        levelend = false;
        kicked = false;
        rotationSpeed = -100.0f;

        //set hero position
        currentSpawn = levelSpawns[levelId];
        hero.transform.position = currentSpawn.transform.position;

        //set cam position
        mainCam.transform.position = levelAxis[levelId].transform.position - new Vector3(0, 1.4f, 11.6f);
        mainCam.transform.eulerAngles = new Vector3(10, 0, 0);

        //set hero turn
        currentTurn = mainCam;
        TurnHero();
        currentY = transform.position.y;
        ninjaSmoke.Stop();

        

        //set scroll position
        scroll.transform.position = levelScrolls[levelId].transform.position;

        //set smoke position
        ninjaSmoke.transform.position = new Vector3(levelScrolls[levelId].transform.position.x, levelScrolls[levelId].transform.position.y - 0.5f, levelScrolls[levelId].transform.position.z);

        //send level start event to analytics
        AnalyticsManager.Instance.SendLevelStartEvent(_compoundLevelIndex+1);

        StartCoroutine(LevelIdle());
    }
    // Update is called once per frame
    void Update()
    {
        if (levelActive)
        {
            hero.transform.RotateAround(levelAxis[levelId].transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
            mainCam.transform.RotateAround(levelAxis[levelId].transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
        }

        if(Input.GetKeyDown("up") && levelActive && !kicked)
        {
            TurnAround();
        }

        if (Input.touchCount > 0 && levelActive && !kicked)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    TurnAround();
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    //touching = false;
                    break;
            }
        }

        if (jumping)
        {
            if(transform.position.y < nextY)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + jumpSpeed * Time.deltaTime, transform.position.z);
                mainCam.transform.position = new Vector3(mainCam.transform.position.x, mainCam.transform.position.y + jumpSpeed * Time.deltaTime, mainCam.transform.position.z);
            } else
            {
                EndJump();
            }
        }

        if (falling)
        {
            if (transform.position.y > nextY)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - jumpSpeed * Time.deltaTime, transform.position.z);
                mainCam.transform.position = new Vector3(mainCam.transform.position.x, mainCam.transform.position.y - jumpSpeed * Time.deltaTime, mainCam.transform.position.z);
            }
            else
            {
                EndFall();
            }
        }

        if (levelend)
        {
            
            //find the vector pointing from our position to the target
            _direction = (mainCam.transform.position - transform.position).normalized;
            //create the rotation we need to be in to look at the target
            _lookRotation = Quaternion.LookRotation(_direction);
            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
        }
    }

    public IEnumerator LevelIdle()
    {
        yield return new WaitForSeconds(idleDuration);
        hero.GetComponent<HeroController>().ActivateLevel();
    }

    private void EndJump()
    {
        jumping = false;
        transform.position = new Vector3(transform.position.x, nextY, transform.position.z);
        mainCam.transform.position = new Vector3(mainCam.transform.position.x, nextCamY, mainCam.transform.position.z);
        currentY = nextY;
        hero.GetComponent<Animator>().SetBool("jump", false);
        hero.GetComponent<Animator>().SetBool("fall", false);
        
    }

    private void EndFall()
    {
        falling = false;
        transform.position = new Vector3(transform.position.x, nextY, transform.position.z);
        mainCam.transform.position = new Vector3(mainCam.transform.position.x, nextCamY, mainCam.transform.position.z);
        currentY = nextY;
        hero.GetComponent<Animator>().SetBool("jump", false);
        hero.GetComponent<Animator>().SetBool("fall", false);
        
    }

    public void ActivateLevel()
    {
        //set hero rotation
        currentTurn = levelTurns[levelId];
        TurnHero();

        levelActive = true;
        hero.GetComponent<Animator>().SetBool("run", true);
    }

    private void TurnHero()
    {
        
        //find the vector pointing from our position to the target
        _direction = (currentTurn.transform.position - transform.position).normalized;
        //create the rotation we need to be in to look at the target
        _lookRotation = Quaternion.LookRotation(_direction);
        //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, 1.0f);
    }

    public void TurnAround()
    {
        rotationSpeed = rotationSpeed * -1;
        hero.transform.Rotate(Vector3.up, 180f);

    }

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == "Spring")
        {
            print("jump");
            jumping = true;
            hero.GetComponent<Animator>().SetBool("jump", true);
            hero.GetComponent<Animator>().SetBool("run", false);
            hero.GetComponent<Animator>().SetBool("fall", false);
            coll.gameObject.GetComponent<Animator>().SetBool("springpop", true);
            StartCoroutine(SpringDown());
            nextY = currentY + 4;
            nextCamY = mainCam.transform.position.y + 4;
            //print("next = " + nextY);
            currentSpring = coll.gameObject;
        }

        if (coll.gameObject.tag == "Hole")
        {
            print("fall");
            falling = true;

            hero.GetComponent<Animator>().SetBool("fall", true);
            hero.GetComponent<Animator>().SetBool("jump", false);
            hero.GetComponent<Animator>().SetBool("run", false);
            
            nextY = currentY - 4;
            nextCamY = mainCam.transform.position.y - 4;
            //print("next = " + nextY);
            
        }

        if (coll.gameObject.tag == "Enemy")
        {
            print("enemy");
            kicked = true;
            coll.gameObject.GetComponent<Animator>().SetBool("kick", true);
            nextY = levelSpawns[levelId].transform.position.y;
            nextCamY = levelAxis[levelId].transform.position.y - 1.4f;
            currentEnemy = coll.gameObject;
            StartCoroutine(EnemyKick());
            //print("next = " + nextY);

        }

        if (coll.gameObject.tag == "LevelEnd")
        {
            print("level end");
            levelActive = false;
            levelend = true;
            hero.GetComponent<Animator>().SetBool("end", true);
            hero.GetComponent<Animator>().SetBool("fall", false);
            hero.GetComponent<Animator>().SetBool("jump", false);
            hero.GetComponent<Animator>().SetBool("run", false);
            StartCoroutine(EndLevel());
        }
    }

    private IEnumerator EnemyKick()
    {
        yield return new WaitForSeconds(0.4f);
        falling = true;
        kicked = false;
        currentEnemy.GetComponent<Animator>().SetBool("kick", false);
        hero.GetComponent<Animator>().SetBool("end", false);
        hero.GetComponent<Animator>().SetBool("fall", true);
        hero.GetComponent<Animator>().SetBool("jump", false);
        hero.GetComponent<Animator>().SetBool("run", false);
    }
    private IEnumerator SpringDown()
    {
        yield return new WaitForSeconds(0.4f);
        currentSpring.GetComponent<Animator>().SetBool("springpop", false);
    }

    private IEnumerator EndLevel()
    {
        
        yield return new WaitForSeconds(3.8f);
        hero.GetComponent<Animator>().SetBool("end", false);
        ninjaSmoke.Play();
        hero.transform.position = new Vector3(transform.position.x, 40.0f, transform.position.z);
        scroll.transform.position = new Vector3(scroll.transform.position.x, 40.0f, scroll.transform.position.z);

        AnalyticsManager.Instance.SendLevelCompleteEvent(_compoundLevelIndex + 1);

        StartCoroutine(NextLevel());
    }

    private IEnumerator ShowTutorial()
    {
        yield return new WaitForSeconds(2.5f);
        tutorialText.SetActive(false);
    }

    private IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(2.0f);
        levels[levelId].SetActive(false);
        levelId = (levelId + 1) % levelSpawns.Length;
        _compoundLevelIndex++;
        print("levelId =" + levelId);
        StartLevel();
    }

}
