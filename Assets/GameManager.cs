using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool levelActive;
    public GameObject hero;
    public static int levelNo;
    public static float idleDuration;
    // Start is called before the first frame update
    void Start()
    {
        
        levelNo = 1;
        idleDuration = 1.0f;
        hero = GameObject.FindGameObjectWithTag("Hero");
        print("hero situation = " + hero.GetComponent<HeroController>().levelActive);
        StartCoroutine(LevelIdle());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator LevelIdle()
    {
        yield return new WaitForSeconds(idleDuration);
        hero.GetComponent<HeroController>().levelActive = true;
        print("hero situation = " + hero.GetComponent<HeroController>().levelActive);
    }
}
