using UnityEngine;

namespace Ernie.Core
{
	public class Singleton<T> : MonoBehaviour	where T : Component
	{
		protected static T _instance;
		public bool DoNotDestroyOnLoad = true;

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<T> ();
					if (_instance == null)
					{
						GameObject obj = new GameObject ();
						//obj.hideFlags = HideFlags.HideAndDontSave;
						_instance = obj.AddComponent<T> ();
					}
				}
				return _instance;
			}
		}

	    protected virtual void Awake ()
	    {
			if (_instance == null)
			{
				_instance = Instance;
				if (DoNotDestroyOnLoad)
				{
					DontDestroyOnLoad(gameObject);
				}
			}
			else if (_instance != this)
			{
				Destroy(gameObject);
			}
		}
	}
}
