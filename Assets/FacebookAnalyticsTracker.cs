﻿using System.Collections;
using System;
using System.Collections.Generic;
using Assets.Ernie.Integration;
using UnityEngine;
#if USE_FACEBOOK_ANALYTICS
using Facebook.Unity;
#endif
using Constants = Ernie.Core.Constants;

namespace Ernie.Integration
{
	public class FacebookAnalyticsTracker : AnalyticsTrackerBase
	{
		public bool DebugMessages = false;
		public static FacebookAnalyticsTracker Instance { get; private set; }

		private void Awake()
		{
			if (Instance != null)
			{
				Destroy(gameObject);
				return;
			}
			else
			{
				Instance = this;
				Init();
			}

			DontDestroyOnLoad(transform.gameObject);
		}

		private void Init()
		{
#if USE_FACEBOOK_ANALYTICS

			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
			else
			{
				FB.Init(() =>
				{
					FB.ActivateApp();
				});
			}
#endif
		}

		public override void SendUserRespondedToLocalNotificationEvent(string notificationName, int notificationHour, int notificationMinute)
		{
			var facebookParams = new Dictionary<string, object>
			{
				{"Label", notificationName} ,
				{"Hour", notificationHour} ,
				{"Minute", notificationMinute} ,
			};

			SendEvent(Constants.AnalyticEvents.USER_RESPONDED_TO_PUSH_NOTIFICATION, "", null, facebookParams);
		}

		public override void SendRewardedAdDisplayedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_SHOWED_KEY);
		}
		public override void SendRewardedAdClickedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_CLICKED_KEY);
		}

		public override void SendRewardedAdDismissedEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_REWARDED_DISMISSED_KEY);
		}

		public override void SendShowInterstitialAdEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_INTERSTITIAL_SHOW_KEY);
		}

		public override void SendShowInterstitalAdRequestEvent()
		{
			SendEvent(Constants.AnalyticEvents.AD_INTERSTITIAL_REQUEST_KEY);
		}

		public override void SendTestEvent()
		{
			SendEvent(Constants.AnalyticEvents.TEST_EVENT_KEY);
		}

		public override void SendLevelCompleteEvent(int levelNo)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);
			var facebookTimeParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(Constants.AnalyticEvents.LEVEL_COMPLETED_KEY, levelName, null, facebookTimeParams);
#endif
		}

		public override void SendLevelSkippedEvent(int levelNo)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);
			var facebookTimeParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(Constants.AnalyticEvents.LEVEL_SKIPPED_KEY, levelName, null, facebookTimeParams);
#endif
		}

		public override void SendLevelQuitEvent(int levelNo)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);

			var facebookTimeParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(Constants.AnalyticEvents.LEVEL_QUIT_KEY, levelName, null, facebookTimeParams);
#endif
		}
		public override void SendLevelStartEvent(int levelNo)//(int level)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);
			var facebookParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(Constants.AnalyticEvents.LEVEL_STARTED_KEY, levelName, null, facebookParams);
#endif
		}

		public override void SendLevelFailEvent(int levelNo)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);
			var facebookParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(Constants.AnalyticEvents.LEVEL_FAILED_KEY, levelName, null, facebookParams);
#endif
		}

		public override void SendRewardedTypeEvent(string rewardType, int levelNo)
		{
#if USE_FACEBOOK_ANALYTICS
			string levelName = GetLevelName(levelNo);
			var facebookParams = new Dictionary<string, object> { ["Level"] = levelName };
			SendEvent(rewardType, levelName, null, facebookParams);
#endif
		}

		protected override void SendEvent(string name)
		{
#if USE_FACEBOOK_ANALYTICS
			try
			{
				FB.LogAppEvent(name);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
#endif
		}

		private void SendEvent(string name, string logString, float? valueToSum, Dictionary<string, object> facebookParams)
		{
#if USE_FACEBOOK_ANALYTICS
			if (!FB.IsInitialized)
			{
				Debug.Log("Trying to send event when FB is not initialized. Not sending event "+name);
				return;
			}

			if (DebugMessages)
			{
				Debug.LogFormat("Facebook Event: {0}, {1}", name, logString);
			}
			try
			{
				FB.LogAppEvent(name, valueToSum, facebookParams);
			}
			catch (Exception e)
			{
				Debug.Log("**** FACEBOOK ANALYTICS EXCEPTION");
				Debug.LogError(e.Message);
			}
#endif
		}
	}
}
