using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public bool levelActive;
    public GameObject hero;
    public int levelNo;
    public float idleDuration;
    // Start is called before the first frame update
    void Start()
    {
        /*levelNo = 1;
        idleDuration = 2.0f;
        hero = GameObject.FindGameObjectWithTag("Hero");
        StartCoroutine(LevelIdle());*/
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator LevelIdle()
    {
        yield return new WaitForSeconds(idleDuration);
        hero.GetComponent<HeroController>().ActivateLevel();
    }
}
