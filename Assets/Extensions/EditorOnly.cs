﻿using UnityEngine;

namespace Ernie.Extensions
{
	public class EditorOnly:MonoBehaviour
	{
		public bool DestroyInEditor = false;
		public void Awake()
		{
#if !UNITY_EDITOR
			Destroy(gameObject);
#else
			if (DestroyInEditor)
			{
				Destroy(gameObject);
			}
#endif
		}
	}
}
