﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Ernie.Extensions
{
    public static class Extensions
    {
        public static Vector3 WithX(this Vector3 v, float x)
        {
            v.x = x;
            return v;
        }

        public static Vector3 WithY(this Vector3 v, float y)
        {
	        v.y = y;
	        return v;
        }

        public static Vector3 WithZ(this Vector3 v, float z)
        {
	        v.z = z;
	        return v;
        }

        public static Bounds GetBounds(this GameObject obj)
        {
	        Bounds bounds;
	        Renderer childRender;
	        bounds = GetRenderBounds(obj);
	        if (bounds.extents.x == 0)
	        {
		        bounds = new Bounds(obj.transform.position, Vector3.zero);
		        foreach (Transform child in obj.transform)
		        {
			        childRender = child.GetComponent<Renderer>();
			        if (childRender != null && childRender.enabled)
			        {
				        bounds.Encapsulate(childRender.bounds);
			        }
			        else
			        {
				        bounds.Encapsulate(child.gameObject.GetBounds());
			        }
		        }
	        }
	        return bounds;
        }

        private static Bounds GetRenderBounds(GameObject obj)
        {
	        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
	        Renderer render = obj.GetComponent<Renderer>();
	        if (render != null && render.enabled)
	        {
		        return render.bounds;
	        }
	        return bounds;
        }

        public static Color WithAlpha(this Color color, float alpha)
        {
	        return new Color
	        {
		        a = alpha,
		        b = color.b,
		        g = color.g,
		        r = color.r
	        };
        }

        public static Vector2 CastToVector2XZ(this Vector3 v)
        {
	        return new Vector2
	        (
		        v.x,
		        v.z
	        );
        }

        public static void Shuffle<T>(this IList<T> list, int seed)
        {
			var rng = new System.Random(seed);
	        int n = list.Count;
	        while (n > 1)
	        {
		        n--;
		        int k = rng.Next(n + 1);
		        T value = list[k];
		        list[k] = list[n];
		        list[n] = value;
	        }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethodName()
        {
	        var st = new StackTrace();
	        var sf = st.GetFrame(1);

	        return sf.GetMethod().Name;
        }

        public static void LogAsNotImplemented()
        {
	        UnityEngine.Debug.Log("Method not implemented: " + GetCurrentMethodName());
        }
	}
}
