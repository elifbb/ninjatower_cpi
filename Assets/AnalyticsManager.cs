﻿using System.Collections;
using System;
using System.Collections.Generic;
using Assets.Ernie.Integration;
using Ernie.Core;
using UnityEngine;
using UnityEngine.UI;
//using com.adjust.sdk;

namespace Ernie.Integration
{
	public enum EventKeys
	{
		level,
		skinUnlock,
	}

	public class AnalyticsManager : Singleton<AnalyticsManager>
	{
		public AnalyticsTrackerBase[] AnalyticsTrackers;
		//[Header("AdjustTokens")]
		//private const string rewardedToken = "gho11c";//"gho11c";
		//private const string interstitialToken = "n2bpgy";//"n2bpgy";
		//private float levelStartTime;

		public void SendUserRespondedToLocalNotificationEvent(string notificationName, int notificationHour, int notificationMinute)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendUserRespondedToLocalNotificationEvent(notificationName, notificationHour, notificationMinute);
			}
		}

		public void SendRewardedAdDisplayedEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendRewardedAdDisplayedEvent();
			}
			//AdjustEvent e = new AdjustEvent(rewardedToken);
			//Adjust.trackEvent(e);
		}
		public void SendRewardedAdClickedEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendRewardedAdClickedEvent();
			}
		}

		public void SendRewardedAdDismissedEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendRewardedAdDismissedEvent();
			}
		}

		public void SendShowInterstitialAdEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendShowInterstitialAdEvent();
			}
			//AdjustEvent e = new AdjustEvent(interstitialToken);
			//Adjust.trackEvent(e);
		}

		public void SendShowInterstitalAdRequestEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendShowInterstitalAdRequestEvent();
			}
		}

		public void SendTestEvent()
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendTestEvent();
			}
			//AdjustEvent e = new AdjustEvent(interstitialToken);
			//Adjust.trackEvent(e);
		}

		public void SendLevelCompleteEvent(int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendLevelCompleteEvent(levelNo);
			}
		}

		public void SendLevelSkippedEvent(int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendLevelSkippedEvent(levelNo);
			}
		}

		public void SendLevelQuitEvent(int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendLevelQuitEvent(levelNo);
			}
		}

		public void SendLevelStartEvent(int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendLevelStartEvent(levelNo);
			}
		}

		public void SendLevelFailEvent(int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendLevelFailEvent(levelNo);
			}
		}

		public void SendRewardedTypeEvent(string rewardType, int levelNo)
		{
			for (int i = 0; i < AnalyticsTrackers.Length; i++)
			{
				AnalyticsTrackers[i]?.SendRewardedTypeEvent(rewardType, levelNo);
			}
		}
	}
}
